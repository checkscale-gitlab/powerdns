PowerDNS in Docker
=========

A docker based PowerDNS role with Traefik option

Requirements
------------

* docker has to be installed
* tested on ubuntu 20.04 and 21.04
* for traefik option traefik has to be deployed by your own

Role Variables
--------------

| Variable | Description | default
| - | - | -
| powerdns_folder | folder where to store data and config | /opt/powerdns
| powerdns_api_key | api token manage via REST api | myrandomkey
| powerdns_db_root_password | mariadb root password (should be vaulted) | r00t!sC00l
| powerdns_db | name of mariadb database | powerdns
| powerdns_db_user | user to connect to database | powerdns
| powerdns_db_password | password for database user (should be vaulted) | my_secret_Passw0rd
| powerdns_publish_api | bool to expose api | true
| powerdns_traefik_deployment | bool to select traefik option | false
| powerdns_loglevel | loglevel for powerdns and recursor | 4
| powerdns_webserver_loglevel | loglevel for webservers | normal
| powerdns_recursor_api_host | Hostname or IP to connect to for api requests | '{{ ansible_default_ipv4.address }}'
| powerdns_recursor_api_port | Port to connec to for api requests | '{% if powerdns_traefik_deployment %}{80{% else 
| powerdns_api_host | Hostname or IP to connect to for api requests | '{{ ansible_default_ipv4.address }}'
| powerdns_api_port | Port to connecto to for api requests | '{% if powerdns_traefik_deployment %}{80{% else %}8081{% e
| powerdns_server_api_over_ssl | bool to select ssl for api requests | false
| powerdns_server_api_ssl_validate_certs | bool to select ssl certificate validation  | 'yes'
| powerdns_recursor_api_over_ssl | bool to select ssl for api requests | false
| powerdns_recursor_api_ssl_validate_certs | bool to select ssl certificate validation | 'yes'
| powerdns_environment | environment variables for powerdns container | `empty`
| powerdns_recursor_environment | environment variables for powerdns recursor container | `empty`

Dependencies
------------

None

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: username.rolename, x: 42 }

License
-------

BSD

Author Information
------------------

Gitlab: https://gitlab.com/public-roles/traefik

Thanks to Ralf Geschke for his inspiration on https://www.kuerbis.org/2020/04/dns-server-mit-powerdns-und-docker-teil-1/ (German)
